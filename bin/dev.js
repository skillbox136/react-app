const express = require('express')
const webpack = require('webpack')
const nodemon = require('nodemon')
const path = require('path')

const [clientConfig, serverConfig] = require('../webpack.config')

const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')

const hmrServer = express()
const clientCompiler = webpack(clientConfig)

hmrServer.use(webpackDevMiddleware(clientCompiler, {
  publicPath: clientConfig.output.publicPath,
  serverSideRender: true,
  noInfo: true,
  watchOptions: {
    ignore: /dist/,
  },
  writeToDisk: true,
  stats: 'errors-only',

}))

hmrServer.use(webpackHotMiddleware(clientCompiler, {
  path: '/static/__webpack_hmr',
}))

hmrServer.listen(3001, () => {
  console.log('server HMR startend in ', 3001)
})

const compiler = webpack(serverConfig)

compiler.run((err) => {
  if (err) {
    console.log(`compilation failed:`, err)
  }
  compiler.watch({}, (err) => {
    if (err) {
      console.log(`compilation failed:`, err)
    }
    console.log('Compilation was successfully')
  });

  nodemon({
    script: path.resolve(__dirname, '../dist/server/server.bundle.js'),
    watch: [
      path.resolve(__dirname, '../dist/server'),
      path.resolve(__dirname, '../dist/client')
    ]
  })
})
