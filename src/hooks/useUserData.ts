import {useEffect, useState} from 'react'
import axios from 'axios'
import { useDispatch, useSelector } from 'react-redux'
import { useToken } from './useToken'
import { IUserData, meRequest, MeRequestAction } from '../redux/me/actions'
import {TRootState} from '../redux/initState'

export function useUserData() {
  useToken()
  const token = useSelector<TRootState>(state => state.token)
  const data = useSelector<TRootState, IUserData>(state => state.me.data)
  const loading = useSelector<TRootState, boolean>(state => state.me.loading)
  const dispatch = useDispatch<any>()
  
  useEffect(() => {
    if (!token) return
    dispatch(meRequest())
  }, [token])

  return {
    data,
    loading
  }
}
