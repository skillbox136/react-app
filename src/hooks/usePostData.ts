import { useEffect, useContext, useState } from "react";
import axios from "axios";
import { useSelector } from "react-redux";
import { useToken } from "./useToken";
import { TRootState } from "../redux/initState";

export interface IPostData {
  id: string 
  title: string 
  author: string
  created: number 
  thumbnail: string 
  score: number 
  subreddit: string
}

export function usePostData() {
  const [postData, setPostData] = useState<IPostData[]>([])
  useToken()
  const token = useSelector<TRootState>(state => state.token)
  const [loading, setLoading] = useState(true)
  const [errorLoading, setErrorLoading] = useState('')

  useEffect(() => {
      setErrorLoading('')
          axios(`https://oauth.reddit.com/best.json`, {
            headers: {Authorization: `bearer ${token}`}
          })
          .then(res => {
            let posts = res.data.data.children.map((item: any) => ({...item.data}))
            setPostData([...posts])
            console.log('post data = ', posts)
          })
          .catch(err => {
            setErrorLoading(err)
          })
          setLoading(false)
    }, [token])
  return [postData]
  // return [postdata, error]
}
