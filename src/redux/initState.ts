import { CardState } from "./cards/cardReducer"
import { MeState } from "./me/meReducer"


export type TRootState = {
  commentText: string
  token: string 
  me: MeState
  cards: CardState
}

export const initialState: any = {
  commentText: '',
  token: '',
  me: {
    loading: false,
    error: '',
    data: {}
  },
  cards: {
    loading: false,
    error: '',
    nextPosts: '',
    countGetPosts: 0,
    data: []
  }

}
