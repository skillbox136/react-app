import { initialState } from "../initState";
import { SetTokenAction } from "./actions";
import * as TYPES from '../types'


export const tokenReducer = (state = initialState.token, {type, payload}: SetTokenAction) => {
  switch(type) {
    case TYPES.SET_TOKEN:
      return payload
    default: 
      return state
  }
}
