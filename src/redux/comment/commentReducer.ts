import { initialState } from "../initState";
import { ReplyCommentAction, UpdateCommentAction } from "./actionComment";
import * as TYPES from '../types'

export type CommentActions = UpdateCommentAction & ReplyCommentAction

export const commentReducer = (state = initialState.commentText, {type, payload}: CommentActions) => {
  switch(type) {
    case TYPES.UPDATE_COMMENT:
      return payload
    case TYPES.REPLY_COMMENT:
      return payload
    default: 
      return state
  }
}
