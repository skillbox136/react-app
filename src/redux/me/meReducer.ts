import * as TYPES from '../types'
import { Reducer } from "react";
import { IUserData, MeRequestAction } from "./actions";
import { initialState } from '../initState';

export type MeState = {
  loading: boolean
  error: string
  data: IUserData
}

type MeActions = MeRequestAction

export const meReducer: Reducer<MeState, MeActions> = (state = initialState.me, {type, payload}) => {
  switch (type) {
    case TYPES.ME_REQUEST:
      return {...state, loading: true}
    case TYPES.ME_REQUEST_SUCCESS:
      return {...state, data: payload, loading: false}
    case TYPES.ME_REQUEST_ERROR:
      return {...state, error: payload, loading: false}
    default: 
      return state
  }
}
