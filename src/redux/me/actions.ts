import { Action, ActionCreator } from 'redux'
import { ThunkAction } from 'redux-thunk'
import axios from 'axios'
import * as TYPES from '../types'
import { Dispatch } from 'react'
import { TRootState } from '../initState'

export type MeRequestAction = {
  type: typeof TYPES.ME_REQUEST | typeof TYPES.ME_REQUEST_SUCCESS | typeof TYPES.ME_REQUEST_ERROR
  payload: any
}

export interface IUserData {
  name?: string
  iconImg?: string
}

export const actionMeRequest: ActionCreator<MeRequestAction> = (payload) => ({
  type: TYPES.ME_REQUEST, payload
})

export const actionMeRequestSuccess: ActionCreator<MeRequestAction> = (payload: IUserData) => ({
  type: TYPES.ME_REQUEST_SUCCESS, payload
})

export const actionMeRequestError: ActionCreator<MeRequestAction> = (payload: string) => ({
  type: TYPES.ME_REQUEST_ERROR, payload
})

export const meRequest = (): ThunkAction<void, TRootState, unknown, Action<string>> => (dispatch: Dispatch<any>, getState) => {

  const token = getState().token
  if (token && token !== 'undefined') {
    dispatch(actionMeRequest())
    axios.get('https://oauth.reddit.com/api/v1/me', {
      headers: { Authorization: `bearer ${token}` }
    })
      .then(res => {
        const userData = res.data
        dispatch(actionMeRequestSuccess({ name: userData.name, iconImg: userData.snoovatar_img })) 
      })
      .catch(err => {
        dispatch(actionMeRequestError(String(err)))
      })
    }
}
