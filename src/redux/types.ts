export const ME_REQUEST: string = 'ME_REQUEST'
export const ME_REQUEST_SUCCESS: string = 'ME_REQUEST_SUCCESS'
export const ME_REQUEST_ERROR: string = 'ME_REQUEST_ERROR'

export const GET_CARDS: string = 'GET_CARDS'
export const GET_CARDS_SUCCESS: string = 'GET_CARDS_SUCCESS'
export const GET_MORE_CARDS_SUCCESS: string = 'GET_MORE_CARDS_SUCCESS'
export const COUNT_POSTS_CARD: string = 'COUNT_POSTS_CARD'
export const GET_CARDS_ERROR: string = 'GET_CARDS_ERROR'

export const UPDATE_COMMENT: string = 'UPDATE_COMMENT'
export const REPLY_COMMENT: string = 'REPLY_COMMENT'

export const SET_TOKEN: string = 'SET_TOKEN'
