import { ActionCreator, Action } from "redux";
import { ThunkAction } from "redux-thunk";
import * as TYPES from '../types'
import axios from 'axios'
import { TRootState } from "../initState";
import { Dispatch } from "react";

export type CardsRequestAction = {
  type: typeof TYPES.GET_CARDS | typeof TYPES.GET_CARDS_SUCCESS | typeof TYPES.GET_MORE_CARDS_SUCCESS | typeof TYPES.COUNT_POSTS_CARD | typeof TYPES.GET_CARDS_ERROR,
  payload: any 
}

export const actionGetCardsRequest: ActionCreator<CardsRequestAction> = (payload) => ({
  type: TYPES.GET_CARDS, payload
})

export const actionGetCardsSuccess: ActionCreator<CardsRequestAction> = (payload) => ({
  type: TYPES.GET_CARDS_SUCCESS, payload
})

export const actionGetMoreCardsRequest: ActionCreator<CardsRequestAction> = (payload: string) => ({
  type: TYPES.GET_MORE_CARDS_SUCCESS, payload
})

export const actionCountPostsCards: ActionCreator<CardsRequestAction> = (payload: number) => ({
  type: TYPES.COUNT_POSTS_CARD, payload
})

export const actionGetCardsError: ActionCreator<CardsRequestAction> = (payload) => ({
  type: TYPES.GET_CARDS_ERROR, payload
})

export const cardsRequest = (): ThunkAction<void, TRootState, unknown, Action<string>> => (dispatch: Dispatch<any>, getState) => {
  const token = getState().token 
  const nextPost = getState().cards.nextPosts

    dispatch(actionGetCardsRequest(true)) // устанавливаем загрузку в true
    dispatch(actionGetCardsError('')) // устанавливает изначально что ошибки нет
    // Переписать UseEffect -> cardsList.ts
    
    axios.get(`${token !== 'undefined' ? 'https://oauth.' : 'https://www.'}reddit.com/best.json`, {
      headers: token !== 'undefined' ? { Authorization: `bearer ${token}` } : {},
      params: {
        limit: 10,
        after: nextPost
      }
    })
    .then(res => {
      let posts = res.data.data.children.map((item: any) => ({ ...item.data }))
      

      console.log('post data Redux = ', posts)
      console.log('nextPosts Redux', nextPost)
      // setLoading(false)
      // setNextAfter(res.data.data.after)
      // setCountAfter(prev => prev += 1)
      dispatch(actionGetMoreCardsRequest(res.data.data.after))
      dispatch(actionCountPostsCards(1))

      dispatch(actionGetCardsSuccess(posts))
      dispatch(actionGetCardsRequest(false))
    })
    .catch(err => {
      dispatch(actionGetCardsError(err))
      dispatch(actionGetCardsRequest(false))
    })
  
}
