import { Reducer } from 'react'
import { initialState } from '../initState'
import * as TYPES from '../types'
import { CardsRequestAction } from './actions'

export type CardState = {
  loading: boolean
  error: string 
  nextPosts: string // нужен для запроса на следующие посты nextAfter
  countGetPosts: number // счетчик (до 3 раз и кнопка)
  data: ICardsData[]
}

export interface ICardsData  {
  id: string
  subreddit: string 
  author: string 
  title: string
  created: number
  thumbnail: string
  score: number
}

type CardsActions = CardsRequestAction

export const cardsReducer: Reducer<CardState, CardsActions> = (state = initialState.cards, {type, payload}) => {
  switch(type) {
    case TYPES.GET_CARDS:
      return {...state, loading: payload}
    case TYPES.GET_CARDS_SUCCESS:
      return {...state, data: [...state.data, ...payload]}
    case TYPES.GET_MORE_CARDS_SUCCESS:
      return {...state, nextPosts: payload}
    case TYPES.COUNT_POSTS_CARD:
      return {...state, countGetPosts: payload === 0 ? payload : state.countGetPosts += payload}
    case TYPES.GET_CARDS_ERROR:
      return {...state, error: payload}
    default: 
      return state
  }

}

