import { combineReducers } from "redux"
import { MeRequestAction } from "./me/actions"
import { SetTokenAction } from "./token/actions"
import { UpdateCommentAction, ReplyCommentAction } from './comment/actionComment'
import { tokenReducer } from "./token/tokenReducer"
import { commentReducer } from "./comment/commentReducer"
import { meReducer } from "./me/meReducer"
import { cardsReducer } from "./cards/cardReducer"


export type MeAction = UpdateCommentAction
| ReplyCommentAction
| SetTokenAction
| MeRequestAction

export const rootReducer = combineReducers({
  token: tokenReducer,
  commentText: commentReducer,
  me: meReducer,
  cards: cardsReducer
})

// переписать CardsList на REDUX 
