import React from 'react'
import { hot } from 'react-hot-loader/root'
import { Layout } from './shared/Layout'
import { Header } from './shared/Header'
import { Content } from './shared/Content'
import { CardsList } from './shared/CardsList'

import './app.global.css'
// import { PostsContextProvider } from './shared/context/postsContext'
import { Provider } from 'react-redux'
import { store } from './redux/store'
import { useToken } from './hooks/useToken'


const AppComponent = () => {
  useToken()

  return (
    <Layout>
      <Header />
      <Content>
        <CardsList />
      </Content>
    </Layout>
  )
}

export const App = hot(() =>
  <Provider store={store}>
    <AppComponent />
  </Provider>
)
