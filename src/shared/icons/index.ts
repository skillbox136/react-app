export * from './MenuIcon'
export * from './BlockIcon'
export * from './WarningIcon'
export * from './CommentIcon'
export * from './SaveIcon'
export * from './SharedIcon'
export * from './IconAnon'

export type TIconSize = 12 | 14 | 16 | 20 | 24 

export interface IIconProps {
  size?: TIconSize
}
