import React from 'react';
import styles from './genericlist.css';

interface IItem {
  id: string,
  text: string,
  className?: string,
  // As?: 'a' | 'li' | 'button' | 'div', добавил в пропс а не в item так логичнее !!
  href?: string,
  onClick?: (id: string) => void
}

interface IGenericListProps {
  list: IItem[],
  As?: 'a' | 'li' | 'button' | 'div',
}

const VOID_FUNC = () => { }

export function GenericList({ list, As = 'div' }: IGenericListProps) {
  return (
    <ul>
      {list.map(({ id, text, className, href, onClick = VOID_FUNC }) => (
        <As
          key={id}
          className={className}
          href={href}
          onClick={() => onClick(id)}
        >
          {text}
        </As>
      ))}
    </ul>
  )
}
