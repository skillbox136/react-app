import React, { createContext, ReactNode } from "react";
import { usePostData, IPostData } from "../../hooks/usePostData";

export const postsContext = createContext<IPostData[]>([])


export function PostsContextProvider({ children }: { children: ReactNode }) {
  const [postData] = usePostData()

  return (
    <postsContext.Provider value={postData}>
      {children}
    </postsContext.Provider>
  )
}
