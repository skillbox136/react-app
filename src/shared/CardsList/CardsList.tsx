import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { actionCountPostsCards, cardsRequest } from '../../redux/cards/actions';
import { TRootState } from '../../redux/initState';
import { Break } from '../Break';
import { Card } from './Card/Card';
import styles from './cardslist.css';

export interface IPostData {
  id: string
  title: string
  author: string
  created: number
  thumbnail: string
  score: number
  subreddit: string
}

export function CardsList() {
  const bottomRef = useRef<HTMLSpanElement>(null)
  const dispatch = useDispatch<any>()
  const token = useSelector<TRootState>(state => state.token)
  const error: any = useSelector<TRootState>(state => state.cards.error)
  const loading = useSelector<TRootState>(state => state.cards.loading)
  const countGetPosts = useSelector<TRootState>(state => state.cards.countGetPosts)
  const nextPost = useSelector<TRootState>(state => state.cards.nextPosts)
  const data: any = useSelector<TRootState>(state => state.cards.data)

  useEffect(() => {
    const observer = new IntersectionObserver((entries) => {
      entries[0].intersectionRatio ? dispatch(cardsRequest()) : null
    }, { rootMargin: '10px', threshold: [0.5] })

    if (bottomRef.current) {
      observer.observe(bottomRef.current)
    }

    return () => {
      bottomRef.current ? observer.unobserve(bottomRef.current) : null
    }
  }, [nextPost, bottomRef.current, token])

  const loadMore = (): void => {
    dispatch(cardsRequest())
    dispatch(actionCountPostsCards(0))
  }

  return (
    <section className={styles.cardSection}>
      <ul className={styles.cardsList} style={{ marginBottom: `${data.length ? '50px' : '0'}` }}>
        {
          data.map((item: IPostData) => (
            <Card
              key={item.id}
              id={item.id}
              subreddit={item.subreddit}
              author={item.author}
              title={item.title}
              created={item.created}
              thumbnail={item.thumbnail}
              score={item.score}
            />
          ))
        }
      </ul>
      {
        countGetPosts === 3
          ? <button onClick={loadMore} className={styles.cardlistBtn}>Загрузить</button>
          : <span ref={bottomRef}></span>
      }
      {
        loading && <h1 className={styles.titleVoid}> &#129300; <Break size={20} top />  Загрузка постов...</h1>
      }
      {
        data.length === 0 && !loading && !error && <h1 className={styles.titleVoid}> &#129300; <Break size={20} top />  Хмм... Здесь пока ничего нет</h1>
      }
      {
        error && error.code !== 'ERR_NETWORK'
          ? <h1 className={styles.titleVoid}> &#129300; <Break size={20} top />  Произошла ошибка!</h1>
          : error && error.code === 'ERR_NETWORK'
            ? <h1 className={styles.titleVoid}> &#129300; <Break size={20} top />  Нет подключения к интернету!</h1>
            : null
      }
    </section>
  );
}
