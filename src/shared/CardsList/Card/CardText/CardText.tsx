import React, { useState } from 'react';
import { Post } from '../../../Post';
import styles from './cardtext.css';

interface ICardTextProps {
  id: string
  author: string
  title: string
  created: number
  subreddit: string
}

export const hoursToWord = (value: number, words: Array<string>) => {
  value = Math.abs(value) % 100
  let num = value % 10
  if (value > 10 && value < 20) return words[2]
  if (num > 1 && num < 5) return words[1]
  if (num === 1) return words[0]
  return words[2]
}

export function CardText({
  id,
  author,
  title,
  created,
  subreddit
}: ICardTextProps): JSX.Element {

  const [isModal, setIsModal] = useState(false)

  created = new Date(created * 1000).getHours()
  const defaultAvatar = 'https://ustanovkaos.ru/wp-content/uploads/2022/02/06-psevdo-pustaya-ava.jpg'



  const onClickTitle = (id: string) => {
    console.log('click title', id)
    setIsModal(true)
  }


  return (
    <div className={styles.textContent}>
      <div className={styles.metaData}>
        <div className={styles.userLink}>
          <img
            className={styles.avatar}
            src={defaultAvatar}
            alt="avatar"
          />
          <a className={styles.username} href="#user-url">{author}</a>
        </div>

        <span className={styles.createdAt}>
          <span className={styles.publishedLabel}>опубликовано </span>
          {created} {hoursToWord(created, ['час', 'часа', 'часов'])} назад
        </span>
      </div>

      <h2 className={styles.title} onClick={() => onClickTitle(id)}>
        <a
          href="#post-url"
          className={styles.postLink}

        >{title}</a>
        {isModal && (
          <Post id={id} subreddit={subreddit} onClose={() => setIsModal(false)} />
        )}
      </h2>
    </div>
  );
}


  // https://cdn.dribbble.com/userupload/2671176/file/original-2adfa2595e746b75fcd316b3241594ef.png?filters:format(webp)?filters%3Aformat%28webp%29=&compress=1&resize=1200x900
