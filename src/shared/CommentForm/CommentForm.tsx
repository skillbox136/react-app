import React, { ChangeEvent, FormEvent, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { updateComment } from '../../redux/comment/actionComment';
import { TRootState } from '../../redux/initState';
import { preventDefault } from '../../utils/react/preventDefault';

import { Formik, Form, Field } from 'formik';

import styles from './commentform.css';
interface ICommentFormProps {
  refAria: any
}

interface IFormicProps {
  commentText: string
}

export function CommentForm({ refAria }: ICommentFormProps) {
  const value = useSelector<TRootState, string>(state => state.commentText)
  const dispatch = useDispatch()

  // const [touched, setTouched] = useState(false)
  // const [valueError, setValueError] = useState('')

  const onHandleSubmit = ({ commentText }: IFormicProps, actionState: any) => {
    // setTouched(true)
    // setValueError(validateValue())
    // if (validateValue()) return
    alert('Комментарии добавлен в Redux State')
    dispatch(updateComment(commentText))
    console.log(commentText)
    actionState.resetForm()
  }

  // const changeTextAria = (e: ChangeEvent<HTMLTextAreaElement>) => {
  //   dispatch(updateComment(e.target.value))
  // }

  // const validateValue = () => value.length < 3 ? 'Введите больше 3-х символов' : ''

  const validateFormik = (text: string) => text.length < 3 ? 'Введите больше 3-х симоволов' : ''

  return (
    <>
      {/* <form
        action=""
        className={styles.form}
        onSubmit={preventDefault(onHandleSubmit)}
      >
        <textarea
          className={styles.input}
          ref={refAria}
          value={value}
          onChange={changeTextAria}
          placeholder={'Оставьте Ваш комментарии'}
          cols={30}
          rows={10}
          aria-invalid={valueError ? 'true' : undefined}
        />
        {
          touched && valueError && <div style={{ color: 'tomato' }}>{valueError}</div>
        }

        <button
          type={'submit'}
          className={styles.button}
        // disabled={isFormValid}
        >
          Комментировать
        </button>
      </form> */}

      <Formik
        initialValues={{
          commentText: ''
        }}
        onSubmit={onHandleSubmit}
      >

        {({ errors, touched, isValidating }) => (
          <Form className={styles.form}>
            <Field
              name="commentText"
              validate={validateFormik}
              as={'textarea'}
              className={styles.input}
              placeholder={'Оставьте Ваш комментарии'}
              aria-invalid={errors.commentText ? 'ture' : undefined}
            />
            {errors.commentText && touched.commentText && <div style={{ color: 'tomato' }}>{errors.commentText}</div>}
            <button type="submit" className={styles.button}>Комментировать</button>
          </Form>
        )}

      </Formik>
    </>
  )
}

