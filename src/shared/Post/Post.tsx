import axios from 'axios';
import React, { useEffect } from 'react';
import { useState } from 'react';
import { useRef } from 'react';
import { createPortal } from 'react-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useOutsideModal } from '../../hooks/useOutsideModal';
import { replyComment } from '../../redux/comment/actionComment';
import { TRootState } from '../../redux/initState';
import { CommentForm } from '../CommentForm';
import { CommentsPost } from '../CommentsPost';
import styles from './post.css';

interface IPostProps {
  id: string
  subreddit: string
  onClose?: () => void | any | undefined
}

const VOID_FUNC = () => { }

export function Post({ id, subreddit, onClose }: IPostProps) {
  // const ref = useRef<HTMLDivElement>(null)
  const refAria = useRef<HTMLTextAreaElement>(null)
  const refAria2 = useRef<HTMLTextAreaElement>(null)
  const [authorPost, setAuthorPost] = useState({})
  const [comments, setComments] = useState([])

  const dispatch = useDispatch()
  const state = useSelector<TRootState, string>(state => state.commentText)
  const token = useSelector<TRootState>(state => state.token)

  const [loading, setLoading] = useState(true)


  useEffect(() => {
    // axios(`https://oauth.reddit.com/comments/${id}`, {
    //   headers: { Authorization: `bearer ${token}` }
    // })
    axios(`https://api.reddit.com/r/${subreddit}/comments/${id}`)
      .then(res => res.data.map((item: any) => item.data.children))
      .then((res) => {
        setAuthorPost(prev => ({ ...prev, ...res[0].map((item: any) => item.data)[0] }))
        setComments(res[1].slice(0, 5).map((item: any) => item.data))
      })
      .catch(console.log)
    setLoading(false)
    return () => {
      // clear ref
      dispatch(replyComment(''))

    }
  }, [])
  const { ref } = useOutsideModal(onClose)

  // useEffect(() => {
  //   const onCloseModal = (e: MouseEvent) => {
  //     if (e.target instanceof Node && !ref.current?.contains(e.target)) {
  //       onClose?.()
  //     }
  //   }
  //   document.addEventListener('click', onCloseModal)
  //   return () => document.removeEventListener('click', onCloseModal)
  // }, [])

  const answerHandler = (name: string): void => {
    refAria.current?.focus()
    dispatch(replyComment(name + ', '))
  }

  const node = document.querySelector('#modal_root')
  if (!node) return null

  return createPortal((
    <div className={styles.modal} ref={ref}>
      <h2>Следует отметить ...</h2>

      <div className={styles.content}>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Impedit repudiandae maiores tempora voluptas culpa! Repellat unde dolorem, fugit dolore officiis excepturi soluta? Culpa maxime architecto blanditiis nobis fuga excepturi temporibus.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, rerum odit, excepturi recusandae, explicabo accusamus assumenda perspiciatis cumque at nostrum placeat illum nihil pariatur quisquam labore ad minima atque vel?</p>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Placeat, maxime assumenda sed corporis dignissimos ea quibusdam et suscipit, ut nisi voluptates sapiente tempore delectus facilis aliquam a, cumque iusto ratione?</p>
        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deleniti nisi voluptate nesciunt eveniet quam, temporibus, minima nulla, odio qui dolore est rerum molestiae magni quibusdam maxime optio omnis aliquam. Fugit.</p>
      </div>

      {
        !loading
          ? <React.Fragment>
            <CommentForm refAria={refAria} />
            {
              comments.map((item: any) =>
                <CommentsPost
                  key={item.id}
                  id={item.id}
                  authorPost={item.author}
                  created={item.created}
                  body={item.body}
                  answerHandler={answerHandler}
                />)
            }</React.Fragment>
          : <div style={{ textAlign: 'center', marginTop: '50px' }}>Необходимо авторизоваться</div>
      }
    </div>
  ), node)
}


