import React, { MouseEventHandler, ReactNode, useEffect, useState } from 'react';
import { useRef } from 'react';
import { createPortal } from 'react-dom';
import styles from './dropdown.css';
import { DropdownList } from './DropdownList';


interface IDropDaownProps {
  button: ReactNode,
  children: ReactNode,
  isOpen?: boolean,
  onOpen?: () => void,
  onClose?: () => void
}

const VOID_FUNC = () => { }


export function Dropdown({
  children,
  button,
  isOpen,
  onOpen = VOID_FUNC,
  onClose = VOID_FUNC
}: IDropDaownProps) {

  const [offset, setOffset] = useState({ top: '0px', left: '0px' })
  const [isDropdownOpen, setIsDropdownOpen] = useState(false)
  const menuRef = useRef<HTMLDivElement>(null)
  const node = document.querySelector('#dropdown_root')
  if (!node) return null

  useEffect(() => setIsDropdownOpen(false), [isOpen])
  useEffect(() => isDropdownOpen ? onOpen() : onClose(), [isDropdownOpen])

  const handleOpen = (e: any) => {
    const { top } = document.body.getBoundingClientRect()
    const btn = e.target.getBoundingClientRect()
    btn
      ? setOffset(prev =>
        ({ ...prev, top: `${Math.ceil(btn.top - top)}px`, left: `${75}vw` }))
      : null
    isOpen ? setIsDropdownOpen(!isDropdownOpen) : null
    // Math.ceil(btn.left - left - 200)
  }

  return (
    <div className={styles.container}>
      <div onClick={handleOpen}>
        {button}
      </div>
      <div >
        {isDropdownOpen && (
          createPortal(
            (<div
              className={styles.listContainer}
              ref={menuRef}
            >
              <div
                className={styles.list}
                style={offset}
                onClick={() => setIsDropdownOpen(false)}
              >
                {children}
              </div>
            </div>), node
          )
        )}
      </div >
    </div>
  )
}
