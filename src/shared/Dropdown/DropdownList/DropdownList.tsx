import React, { ReactNode, useEffect } from 'react';
import { useState } from 'react';
import { createPortal } from 'react-dom';
import styles from './dropdownlist.css';

interface IDropDownProps {
  children: ReactNode
  // open: boolean
  // isOpen: (value: boolean) => void
}

export function DropdownList({ children }: IDropDownProps) {
  const node = document.querySelector('#dropdown_root')
  if (!node) return null

  return createPortal((
    <div className={'portalParent'}>
      {children}
    </div>

  ), node);
}
