import * as React from 'react'
import { getValue } from '../utils/react/pickFromCurrentTarget'
import { preventDefault } from '../utils/react/preventDefault'
import { stopPropagation } from '../utils/react/stopPropagation'

function InputExample({ value, onChange }: any) {
  return (
    <input
      value={value}
      onChange={pipe(preventDefault, stopPropagation, getValue, onChange)}
    />
  )
}


function compose<U>(...fns: Function[]) {
  return <E,>(initialValue: any): U =>
    fns.reduceRight((prevValue, fn) => fn(prevValue), initialValue)
}

function pipe<U>(...fns: Function[]) {
  return <E,>(initialValue: any): U =>
    fns.reduce((prevValue, fn) => fn(prevValue), initialValue)
}



function pick<K extends string>(prop: K) {
  return <O extends Record<K, unknown>>(obj: O) => obj[prop] // unknown -> any 
}

function isEqual<T>(left: T) {
  return <E extends T>(right: E) => left === right
}

function cond(b: boolean) {
  return !b
}

// const some = pick('value')({value: 2}) // 2

const comments = [{ id: 11, text: 'textOne' }, { id: 23, value: 'textTwo' }]
const createFilterBy = (prop: string) => (id: number) => pipe(pick(prop), isEqual(id), cond)
const filterWithId = createFilterBy('id')
const filterByValue = createFilterBy('value')


const filteredComments = comments.filter(filterWithId(22))




